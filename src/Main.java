import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner ( System.in ) ;
        System.out.print ( " Masukkan Poin Tukar : " ) ;
        String poinTukar = scan.nextLine ( ) ;
        System.out.print ( " Masukkan Poin : " ) ;
        int n = scan.nextInt ( ) ;
        String [ ] splitPoinTukar = poinTukar.split ( " " ) ;
        int [ ] poins = new int [ splitPoinTukar.length ] ;
        for ( int i = 0 ; i < splitPoinTukar.length ; i ++ ) {
            poins [ i ] = Integer.parseInt ( splitPoinTukar [ i ] ) ;
        }
        System.out.println ( " n : " + n ) ;
        printPoins ( poins ) ;
        int total = poinTukarGreedy ( poins , n ) ;
        System.out.println("poin " + n +" dapat ditukar menjadi " + total + " voucher");
    }
    public static int poinTukarGreedy ( int [ ] poins , int n ) {
        int total = 0;
        while (n > 100) {
            for (int i = poins.length - 1; i >= 0; i--) {
                if (poins[i] <= n) {
                    n -= poins[i];
                    System.out.println(" Pilih Poin Tukar " + poins[i] + "p" + " tersisa " + n);
                    i++;
                    total++;
                }
            }
        }
        return total;
    }
    public static void printPoins ( int [ ] poins ) {
        System.out.println ( " Poin Tukar : " ) ;
        for ( int poin : poins ) {
            System.out.print (poin + "p" + ", ");
        }
        System.out.println("\n") ;
    }
}
